﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Reflection;
using System.Resources;
using CefSharp;
using System.Runtime.InteropServices;
using System.Threading;
using System.Diagnostics;

namespace BashkortTele
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();

			DirectoryInfo appDir, booksDir;

			try {
				appDir = new FileInfo(Assembly.GetExecutingAssembly().Location).Directory
					.GetDirectories("application")[0];
				booksDir = appDir.GetDirectories("books")[0];

				if (appDir.GetFiles("index.html").Length == 0) {
					throw new Exception("Нарушена целостность Электронного Учебника. Переустановка приложения может решить проблему.");
				}
			} catch (Exception ex) {
				MessageBox.Show(ex.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);

				Hide();
				Application.Current.Shutdown(1);
				return;
			}

			BrowserSettings browserSettings = new BrowserSettings() {
				LocalStorageDisabled = false,
				FileAccessFromFileUrlsAllowed = true
			};
			Browser.BrowserSettings = browserSettings;

			Browser.Address = appDir.GetFiles("index.html")[0].FullName;

			Browser.RegisterJsObject("bridge", new Bridge(appDir, this));
			Browser.RegisterJsObject("mediaPlayer", new MediaPlayer(appDir));
			Browser.RegisterJsObject("booksManager", new BooksManager(booksDir, Browser));
			Browser.ConsoleMessage += (sender, e) => {
				Debug.WriteLine("[{0}:{1}] {2}", e.Source, e.Line, e.Message);
			};
			/*Browser.ContextMenu = new ContextMenu();
			MenuItem showInspector = new MenuItem();
			showInspector.Header = "Показать инспектор";
			showInspector.Click += (sender, e) => {
				Browser.ShowDevTools();
			};
			Browser.ContextMenu.Items.Add(showInspector);*/
		}

		[DllImport("user32.dll")]
		[return: MarshalAs(UnmanagedType.Bool)]
		private static extern bool SetForegroundWindow(IntPtr hWnd);

		[STAThread]
		public static void Main(string[] args)
		{
			bool createdNew = true;
			using (Mutex mutex = new Mutex(true, "FRKIKBashkortTele", out createdNew)) {
				if (createdNew) {
					BashkortTele.App app = new BashkortTele.App();
					app.InitializeComponent();
					app.Run();
				} else {
					Process current = Process.GetCurrentProcess();
					foreach (Process process in Process.GetProcessesByName(current.ProcessName)) {
						if (process.Id != current.Id) {
							SetForegroundWindow(process.MainWindowHandle);
							break;
						}
					}
				}
			}
		}
	}
}
