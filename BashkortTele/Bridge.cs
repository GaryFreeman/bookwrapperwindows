﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace BashkortTele
{
	/// <summary>
	/// Класс главного объекта-моста между .NET и JS.
	/// </summary>
	public class Bridge
	{
		private DirectoryInfo appDir;
		private MainWindow window;

		/// <summary>
		/// Конструктор.
		/// </summary>
		/// <param name="appDir">Директория web-приложения</param>
		public Bridge(DirectoryInfo appDir, MainWindow window)
		{
			this.appDir = appDir;
			this.window = window;
		}

		/// <summary>
		/// Возвращает путь к web-приложению.
		/// </summary>
		/// <returns>Пусть к web-приложению</returns>
		public string GetApplicationPath()
		{
			return appDir.FullName;
		}

		public void Exit()
		{
			window.Dispatcher.BeginInvoke((Action)(() => {
				window.Close();
			}));
		}
	}
}
