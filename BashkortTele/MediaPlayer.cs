﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using NAudio;
using NAudio.Wave;
using NAudio.Wave.SampleProviders;
using System.IO;

namespace BashkortTele
{
	/// <summary>
	/// Медиа плеер для воспроизведения звуков в учебнике.
	/// </summary>
	public class MediaPlayer
	{
		private DirectoryInfo appDir;
		private Dictionary<string, WaveOut> players = new Dictionary<string, WaveOut>();
		private Dictionary<string, WaveStream> waveStreams = new Dictionary<string, WaveStream>();

		/// <summary>
		/// Конструктор.
		/// </summary>
		/// <param name="appDir">Директория web-приложения</param>
		public MediaPlayer(DirectoryInfo appDir)
		{
			this.appDir = appDir;
		}

		/// <summary>
		/// Воспроизводит указанный файл.
		/// </summary>
		/// <param name="url">Путь к файлу от корня web-приложения</param>
		/// <param name="id">Идентификатор звука</param>
		public void play(string url, string id)
		{
			if (players.ContainsKey(id)) {
				if (players[id].PlaybackState == PlaybackState.Paused) {
					players[id].Resume();
				}

				return;
			}

			var file = new FileInfo(appDir.FullName + '/' + url);
			if (file.Exists) {
				var waveStream = new AudioFileReader(file.FullName);
				var audioFileReader = new AutoDisposeFileReader(waveStream);
				var player = new WaveOut();

				player.PlaybackStopped += (sender, e) => {
					players.Remove(id);
					waveStreams.Remove(id);
				};

				player.Init(audioFileReader);
				player.Play();

				players.Add(id, player);
				waveStreams.Add(id, waveStream);
			}
		}

		/// <summary>
		/// Временно останавливает воспроизведение файла.
		/// </summary>
		/// <param name="id">Идентификатор звука</param>
		public void pause(string id)
		{
			if (players.ContainsKey(id)) {
				players[id].Pause();
			}
		}

		/// <summary>
		/// Останавливает воспроизведение файла.
		/// </summary>
		/// <param name="id">Идентификатор звука</param>
		public void stop(string id)
		{
			if (players.ContainsKey(id)) {
				players[id].Stop();
			}
		}

		/// <summary>
		/// Останавливает воспроизведение всех файлов.
		/// </summary>
		public void stopAll()
		{
			var keys = players.Keys.ToList();

			foreach (var k in keys) {
				stop(k);
			}
		}

		public double GetVolume(string id)
		{
			if (players.ContainsKey(id)) {
				return players[id].Volume;
			}

			return 0;
		}

		public void SetVolume(string id, double volume)
		{
			if (players.ContainsKey(id)) {
				players[id].Volume = (float)volume;
			}
		}

		public double GetPosition(string id)
		{
			if (waveStreams.ContainsKey(id)) {
				return waveStreams[id].CurrentTime.TotalSeconds / waveStreams[id].TotalTime.TotalSeconds;
			}

			return 0;
		}

		public void SetPosition(string id, double position)
		{
			if (waveStreams.ContainsKey(id)) {
				waveStreams[id].CurrentTime = TimeSpan.FromSeconds(waveStreams[id].TotalTime.TotalSeconds * position);
			}
		}

		public int GetState(string id)
		{
			if (players.ContainsKey(id)) {
				return (int)players[id].PlaybackState;
			}

			return 0;
		}

		public double GetDuration(string id)
		{
			if (waveStreams.ContainsKey(id)) {
				return waveStreams[id].TotalTime.TotalSeconds;
			}
			
			return 0;
		}
	}
}
