﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using System.IO;
using CefSharp;

namespace BashkortTele
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		public App()
		{

			Cef.Initialize(new CefSettings() {
				Locale = "ru",
				CachePath = "cache",
				LogSeverity = LogSeverity.Disable
			});
		}
	}
}
