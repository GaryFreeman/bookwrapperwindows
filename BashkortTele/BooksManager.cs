﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Diagnostics;
using CefSharp;
using CefSharp.Wpf;
using Newtonsoft.Json;

namespace BashkortTele
{
	/// <summary>
	/// Менеджер учебников. Позволяет получить информацию об установленных учебниках, а также подпи
	/// </summary>
	public class BooksManager : IDisposable
	{
		private DirectoryInfo booksDir;
		private ChromiumWebBrowser browser;
		private Dictionary<string, string> handlers = new Dictionary<string, string>();
		private Dictionary<string, Dictionary<string, object>> books =
			new Dictionary<string, Dictionary<string, object>>();
		private FileSystemWatcher watcher = new FileSystemWatcher();
		private Dictionary<string, FileSystemWatcher> bookWatchers =
			new Dictionary<string, FileSystemWatcher>();

		public event EventHandler BooksChanged;

		/// <summary>
		/// Конструктор.
		/// </summary>
		/// <param name="booksDir">Директория учебников</param>
		/// <param name="browser">Браузер</param>
		public BooksManager(DirectoryInfo booksDir, ChromiumWebBrowser browser)
		{
			this.booksDir = booksDir;
			this.browser = browser;

			LoadBooks();
			//InitWatching();
		}

		/// <summary>
		/// Возвращает информацию об установленных учебниках.
		/// </summary>
		/// <returns>JSON-строка с информацией об учебниках</returns>
		public string Books()
		{
			return JsonConvert.SerializeObject(books);
		}

		/// <summary>
		/// Добавляет обработчик события изменения списка учебников.
		/// </summary>
		/// <param name="handlerName">Название обработчика</param>
		/// <param name="code">JS-код обработчика</param>
		public void OnBooksChanged(string handlerName, string code)
		{
			handlers[handlerName] = code;
		}

		/// <summary>
		/// Удаляет обработчик изменения списка учебников.
		/// </summary>
		/// <param name="handlerName">Название обработчика</param>
		public void RemoveBooksChangedHandler(string handlerName)
		{
			handlers.Remove(handlerName);
		}

		/// <summary>
		/// Загружает информацию об учебниках с диска.
		/// </summary>
		private void LoadBooks()
		{
			books.Clear();

			DirectoryInfo[] bookDirs = booksDir.GetDirectories();
			foreach (var bd in bookDirs) {
				var bookXML = bd.GetFiles("book.xml", SearchOption.TopDirectoryOnly);
				if (bookXML.Length != 0) {
					var bookInfo = LoadBook(bookXML[0]);
					books[bookInfo["code"] as String] = bookInfo;
				}
			}
		}

		/// <summary>
		/// Загружает информацию о конкретном учебнике учебнике.
		/// </summary>
		/// <param name="bookXML">Файл book.xml с мета-данными учебника</param>
		/// <returns>Информацию об учебнике</returns>
		private Dictionary<string, object> LoadBook(FileInfo bookXML)
		{
			var info = new Dictionary<string, object>();

			using (StreamReader file = File.OpenText(bookXML.FullName))
			using (XmlReader reader = new XmlTextReader(file)) {
				reader.MoveToContent();

				if (reader.IsStartElement() && reader.Name == "book") {
					reader.ReadStartElement();
					reader.MoveToContent();

					while (!reader.EOF) {
						if (reader.IsStartElement()) {
							var name = reader.Name;

							if (name == "pages") {
								List<Dictionary<string, string>> pages = new List<Dictionary<string, string>>();
								XmlReader pagesReader = reader.ReadSubtree();

								pagesReader.ReadStartElement();
								pagesReader.MoveToContent();

								while (!pagesReader.EOF && pagesReader.NodeType != XmlNodeType.EndElement) {
									Dictionary<string, string> page = new Dictionary<string, string>();

									pagesReader.ReadStartElement();
									pagesReader.MoveToContent();

									while (pagesReader.NodeType != XmlNodeType.EndElement) {
										string elName = pagesReader.Name;
										pagesReader.ReadStartElement();
										pagesReader.MoveToContent();

										string elVal = pagesReader.ReadContentAsString();
										page[elName] = elVal;

										pagesReader.MoveToContent();
										pagesReader.ReadEndElement();
										pagesReader.MoveToContent();
									}

									pagesReader.ReadEndElement();
									pagesReader.MoveToContent();

									pages.Add(page);
								}

								pagesReader.ReadEndElement();
								pagesReader.Close();

								info[name] = pages;
							} else {
								var value = reader.ReadElementContentAsString();
								info[name] = value;
							}
						} else {
							reader.ReadEndElement();
						}

						reader.MoveToContent();
					}
				}
			}

			return info;
		}

		/// <summary>
		/// Инициализирует наблюдение за изменениями учебников.
		/// </summary>
		private void InitWatching()
		{
			watcher.Path = booksDir.FullName;
			watcher.NotifyFilter = NotifyFilters.DirectoryName;
			watcher.Created += BooksFolderChanged;
			watcher.Deleted += BooksFolderChanged;
			watcher.Renamed += BooksFolderChanged;
			watcher.EnableRaisingEvents = true;
		}

		/// <summary>
		/// Обработчик изменения списка директорий учебников.
		/// </summary>
		private void BooksFolderChanged(object sender, FileSystemEventArgs e)
		{
			switch (e.ChangeType) {
				case WatcherChangeTypes.Created:
					var bookDir = new DirectoryInfo(e.FullPath);
					var bookXML = bookDir.GetFiles("book.xml");
					if (bookXML.Length != 0) {
						var bookInfo = LoadBook(bookXML[0]);
						books[bookInfo["code"] as String] = bookInfo;
					}

					FileSystemWatcher bookWatcher = new FileSystemWatcher(bookDir.FullName);
					bookWatcher.NotifyFilter = NotifyFilters.LastWrite;
					bookWatcher.Changed += BookChanged;
					bookWatcher.EnableRaisingEvents = true;
					bookWatchers[bookDir.Name] = bookWatcher;
					break;
				case WatcherChangeTypes.Deleted:
					var name = new DirectoryInfo(e.FullPath).Name;
					books.Remove(name);
					bookWatchers[name].EnableRaisingEvents = false;
					bookWatchers[name].Dispose();
					bookWatchers.Remove(name);
					break;
				case WatcherChangeTypes.Renamed:
					name = new DirectoryInfo(e.FullPath).Name;
					if (books.ContainsKey(name)) {
						books.Remove(name);

						bookDir = new DirectoryInfo(e.FullPath);
						bookXML = bookDir.GetFiles("book.xml");
						if (bookXML.Length != 0) {
							var bookInfo = LoadBook(bookXML[0]);
							books[bookInfo["code"] as String] = bookInfo;
						}
					}
					break;
			}
		}

		/// <summary>
		/// Обработчик события изменения учебника директории учебника.
		/// </summary>
		private void BookChanged(object sender, FileSystemEventArgs e)
		{
			var name = new FileInfo(e.FullPath).Directory.Name;
			if (books.ContainsKey(name)) {
				books.Remove(name);

				var bookDir = new DirectoryInfo(e.FullPath);
				var bookXML = bookDir.GetFiles("book.xml");
				if (bookXML.Length != 0) {
					var bookInfo = LoadBook(bookXML[0]);
					books[bookInfo["code"] as String] = bookInfo;
				}
			}
		}

		/// <summary>
		/// Генерирует событие изменения учебников (вызывает JS-обработчики).
		/// </summary>
		private void RaiseBooksChanged()
		{
			foreach (var h in handlers) {
				browser.ExecuteScriptAsync(h.Value);
			}

			if (BooksChanged != null) {
				BooksChanged(this, new EventArgs());
			}
		}

		public void Dispose()
		{
			watcher.EnableRaisingEvents = false;
			watcher.Dispose();

			foreach (var w in bookWatchers) {
				w.Value.EnableRaisingEvents = false;
				w.Value.Dispose();
			}

			bookWatchers.Clear();
		}
	}
}
